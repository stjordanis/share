# SHARE

Installs file sharing and locking capabilities on your hard disk - for FreeDOS kernel only


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## SHARE.LSM

<table>
<tr><td>title</td><td>SHARE</td></tr>
<tr><td>version</td><td>08/2006a</td></tr>
<tr><td>entered&nbsp;date</td><td>2007-10-20</td></tr>
<tr><td>description</td><td>Installs file sharing and locking capabilities on your hard disk - for FreeDOS kernel only</td></tr>
<tr><td>keywords</td><td>share,file sharing</td></tr>
<tr><td>author</td><td>Ron Cemer</td></tr>
<tr><td>maintained&nbsp;by</td><td>Michael Devore (FreeDosStuff -AT- devoresoftware.com) Japheth &lt;mail -at- japheth.de&gt;</td></tr>
<tr><td>primary&nbsp;site</td><td>https://github.com/FDOS/share</td></tr>
<tr><td>platforms</td><td>FreeDOS only</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License](LICENSE)</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Share</td></tr>
<tr><td>legacy&nbsp;site</td><td>http://www.dosemu.org/~bart/</td></tr>
<tr><td>legacy&nbsp;site</td><td>ftp://ftp.devoresoftware.com/downloads/share.zip</td></tr>
</table>
